import numpy as np
from keras.preprocessing import image
from matplotlib import pyplot as plt

test_image = image.load_img('Advance_Crocin_advance_13901.bmp', target_size=(64, 64))
plt.imshow(test_image)
plt.show()
test_image = image.img_to_array(test_image)
test_image = np.expand_dims(test_image, axis=0)
