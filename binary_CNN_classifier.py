from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Dropout
from keras.layers import Flatten
from keras.layers import Dense

from keras.preprocessing.image import ImageDataGenerator

import numpy as np
from keras.preprocessing import image
from matplotlib import pyplot as plt

classifier = Sequential()

classifier.add(Conv2D(32, (3, 3), input_shape=(64, 64, 3), activation='relu'))
classifier.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

classifier.add(Conv2D(32, (3, 3), activation='relu'))
classifier.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))



classifier.add(Flatten())

classifier.add(Dense(units=128, activation='relu'))
model.add(Dropout(0.5))
classifier.add(Dense(units=1, activation='sigmoid'))

classifier.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

train_datagen = ImageDataGenerator(rescale=1. / 255)

test_datagen = ImageDataGenerator(rescale=1. / 255)

training_set = train_datagen.flow_from_directory('Advance_Crocin_advance_training',
                                                 target_size=(64, 64),
                                                 batch_size=32,
                                                 class_mode='binary')

print(training_set)

print("\n")

test_set = test_datagen.flow_from_directory('Advance_Crocin_advance_validation',
                                            target_size=(64, 64),
                                            batch_size=32,
                                            class_mode='binary')

print(test_set)

print("\n")

classifier.fit_generator(training_set,
                         steps_per_epoch=28,
                         epochs=10,
                         validation_data=test_set,
                         validation_steps=2)

test_image = image.load_img('Advance_Crocin_advance_13901.bmp', target_size=(64, 64))
plt.imshow(x)
test_image = image.img_to_array(test_image)
test_image = np.expand_dims(test_image, axis=0)
result = classifier.predict(test_image)

print("\nTraining set indices : {}".format(training_set.class_indices))
print("\nTest set indices : {}".format(test_set.class_indices))

res = classifier.evaluate(training_set, test_set,
                          batch_size=32, verbose=1, show_accuracy=True)

if result[0][0] == 1:
    prediction = 'Crocin Advance'
else:
    prediction = 'Not Crocin Advance'

print("\nPrediction : {}".format(prediction))
print('Test accuracy: {0}'.format(res[1]))
